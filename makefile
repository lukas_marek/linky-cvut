#Author: Lukáš Marek
#Project: Party Outvaders

name = linky
installationDir = /home/marek/.config/GIMP/2.10/plug-ins/linky/#change this according to yours gimp plug-in folder (beware of whitespaces at the end, thus this comment)
buildDir = build
flags = -pedantic -Wall -Wextra -I public/
libraries = `gimptool-2.0 --cflags` `gimptool-2.0 --libs` -lcurl
objectFiles = $(buildDir)/main.o $(buildDir)/curlLinky.o $(buildDir)/framesLinky.o $(buildDir)/dialogLinky.o

compile: $(objectFiles)
	g++ $(flags) $(objectFiles) -o $(name) $(libraries)
	mkdir -p $(installationDir)
	mv $(name) $(installationDir)

$(buildDir)/%.o: private/%.cpp
	make replaceInstallationPath
	mkdir -p $(buildDir)
	g++ $(flags) $< -c -o $@ $(libraries)

replaceInstallationPath:
	echo $(installationDir) > installationDir
	./replaceInstallationPath.sh
	rm installationDir

clean:
	rm -rf $(buildDir)

install:
	make compile
	make clean
