#pragma once

struct TdialogVals
{
    TdialogVals();
    //save current dialog values into a file
    void save();
    //load last dialog values from a file
    void load();

    gint fps;
    gboolean debug;
    gboolean clearBuffer;
    std::string user_id;
    std::string user_secret;

private:
    //checks if a file exists
    bool fileExists(const std::string &name);
    std::string installationPath;
};

gboolean dialog(TdialogVals *dialogVals);