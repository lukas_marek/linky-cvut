#pragma once
#include <iostream>
#include <nlohmann/json.hpp>

//complementary function
static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp);

//returns token needed for accessing linky
std::string curlLinkyTokenCreate(const std::string &user_id, const std::string &user_secret, bool debug = true, int fps = 22);

//sends data to Linky
bool curlLinkyPlay(const std::string &access_token, const nlohmann::json &frames, bool clear_buffer = true);